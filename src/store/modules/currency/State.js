export default class State {
    collection = {}
    faits =  [
        { id: 1, name: "UAH", },
        { id: 2, name: "USD", },
        { id: 3, name: "EUR", },
        { id: 4, name: "JPY", },
        { id: 5, name: "CAD", },
        { id: 6, name: "CHF", },
        { id: 7, name: "GBP", },
    ]
}
