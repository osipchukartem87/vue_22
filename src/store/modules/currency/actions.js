import { FETCH_CURRENCY } from "./types/actions";
import { SET_CURRENCY } from "./types/mutations";

export function getActions() {
    return {
        [FETCH_CURRENCY]({ commit, }) {
            fetch("https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD")
            .then((response) => {
                return response.text();
              })
              .then((data) => {
                console.log(data);
              });
            return new Promise((resolve) => {
                setTimeout(() => {
                    commit(SET_CURRENCY,
                        {
                            BTC: {
                                USD: 1000,
                            },
                            ETH: {
                                UAH: 100000,
                            },
                        }
                    );
                    resolve();
                }, 500);
            });
        },
    };
}
