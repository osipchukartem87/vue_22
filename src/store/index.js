import Vue from "vue";
import Vuex from "vuex";
import { getCurrencyModule } from "@/store/modules/currency";
import Fragment from "vue-fragment";

Vue.use(Vuex);
Vue.use(Fragment.Plugin);

export default new Vuex.Store({
    strict: true,
    modules: {
        currency: getCurrencyModule(),
    },
});
